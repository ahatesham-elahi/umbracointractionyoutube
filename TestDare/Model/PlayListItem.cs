﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestDare.Model
{
    public class PlayListItem
    {

        public string Title { get; set; }
        public string VideoUrl { get; set; }
        public string ThumpnailImage { get; set; }
        public string VideoId { get; set; }
    }
}
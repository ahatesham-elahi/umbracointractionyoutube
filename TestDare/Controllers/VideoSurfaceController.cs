﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Umbraco.Web.Mvc;
using TestDare.Model;

namespace TestDare.Controllers
{
    public class VideoSurfaceController : SurfaceController
    {
        UserCredential credential;


        // GET: VideoSurface
        public IHtmlString RederListView()
        {
            // This method authorizes the YouTube registered User
            Authrize().Wait();
 
            //Calling AddVideos method to add Videos in Umbraco
            //GerList method is for fetching the playlist of the user from YouTube

            AddVideos(GetList());
            return Umbraco.RenderMacro("DisplayVideoList");
        }


        private async Task Authrize()
        {
            string filePath = Server.MapPath(Url.Content("~/Json/youtube.json"));

            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    // This OAuth 2.0 access scope allows for read-only access to the authenticated 
                    // user's account, but not other types of account access.
                    new[] { YouTubeService.Scope.YoutubeReadonly },
                    "user",
                    CancellationToken.None,
                    new FileDataStore(this.GetType().ToString())
                );
            }
        }


        private List<PlayListItem> GetList()
        {

            
            // declaring the variable to save the list of videos fetched from YouTube Playlist
            List<PlayListItem> playList = new List<PlayListItem>();
            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Google.Apis.YouTube.Samples.PlaylistUpdates",
            });

            var channelsListRequest = youtubeService.Channels.List("contentDetails");
            channelsListRequest.Mine = true;

            // Retrieve the contentDetails part of the channel resource for the authenticated user's channel.
            var channelsListResponse =  channelsListRequest.Execute();

            foreach (var channel in channelsListResponse.Items)
            {
                // From the API response, extract the playlist ID that identifies the list
                // of videos uploaded to the authenticated user's channel.
                var uploadsListId = channel.ContentDetails.RelatedPlaylists.Uploads;

                Console.WriteLine("Videos in list {0}", uploadsListId);

                var nextPageToken = "";
                while (nextPageToken != null)
                {
                    var playlistItemsListRequest = youtubeService.PlaylistItems.List("snippet");
                    
                    playlistItemsListRequest.PlaylistId = "PL1J8WVAVtJJ3KKoISBeiFZvaFrNW0WL6G";
                    playlistItemsListRequest.MaxResults = 50;
                    playlistItemsListRequest.PageToken = nextPageToken;

                    // Retrieve the list of videos uploaded to the authenticated user's channel.
                    var playlistItemsListResponse =  playlistItemsListRequest.Execute();
                    var videoBaseUrl = "https://www.youtube.com/watch?v=";

                    foreach (var playlistItem in playlistItemsListResponse.Items)
                    {
                        playList.Add(new PlayListItem { Title = playlistItem.Snippet.Title,
                            ThumpnailImage = playlistItem.Snippet.Thumbnails.Default__.Url,
                            VideoUrl = videoBaseUrl+playlistItem.Snippet.ResourceId.VideoId,
                            VideoId= playlistItem.Snippet.ResourceId.VideoId
                        });
                        // Print information about each video.
                        Console.WriteLine("{0} ({1})", playlistItem.Snippet.Title, playlistItem.Snippet.Description);
                    }

                    nextPageToken = playlistItemsListResponse.NextPageToken;
                }
            }

            return playList;
        }


        public void AddVideos(List<PlayListItem> items)
        {
            var contentService = Services.ContentService;
            var content = Umbraco.Content(1147);
            var listVideos = content.Children.ToList();


            var ids = listVideos.Select(e => 
             e.GetProperty("videoId").GetValue()
            ).ToList();

            var filterResults = items.Where(m => ! ids.Contains(m.VideoId)).ToList();


            foreach (var item in filterResults)
            {
                
                var videoContent = contentService.CreateAndSave("YouTubeVideo", 1147, "videoItem");
                videoContent.SetValue("VideoDescription", item.Title);
                videoContent.SetValue("VideoThumbnail", item.ThumpnailImage);
                videoContent.SetValue("VideoUrl", item.VideoUrl);
                videoContent.SetValue("videoId", item.VideoId);
                contentService.SaveAndPublish(videoContent);
            }
        }



    }



}